# https://github.com/bikcrum/ppo_transformer/blob/main/network.py

import torch
import torch.nn as nn
import math
from typing import List
##################################################################################################
##################################################################################################

def _weight_norm(module):
    nn.init.orthogonal_(module.weight)
    module.bias.data.zero_()
    return nn.utils.weight_norm(module)

class PositionalEncoding(nn.Module):
    def __init__(self, d_model: int, dropout: float = 0.1, max_len: int = 5000):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)
        position = torch.arange(max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * (-math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, 1, d_model)
        pe[:, 0, 0::2] = torch.sin(position * div_term)
        pe[:, 0, 1::2] = torch.cos(position * div_term)
        self.register_buffer('pe', pe)  # x: [seq_len, batch_size, embedding_dim]
    def forward(self, x):
        x = x + self.pe[:x.size(0)]
        return self.dropout(x)
_n_light_axix = 24
_activation = nn.ReLU()
_pool = nn.MaxPool2d(2)
_padding_mode = "reflect"


##################################################################################################
###   Feature Extractor   ###
class ImageConv(nn.Module):
  def __init__(self):
    super().__init__()
    self.conv_a1 = _weight_norm(nn.Conv2d(3, 32, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a2 = _weight_norm(nn.Conv2d(32, 64, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a3 = _weight_norm(nn.Conv2d(64, 64, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a4 = _weight_norm(nn.Conv2d(64, 128, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a5 = _weight_norm(nn.Conv2d(128, 128, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a6 = _weight_norm(nn.Conv2d(128, 256, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a7 = _weight_norm(nn.Conv2d(256, 256, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a8 = _weight_norm(nn.Conv2d(256, 256, 3, padding=1, padding_mode=_padding_mode))

  def forward(self, x):
    n_batch = x.shape[0]
    n_max_images = x.shape[1]
    x = x.view(n_batch * n_max_images, x.shape[2], x.shape[3], x.shape[4])
    x = _pool(_activation(self.conv_a1(x)))
    x = _activation(self.conv_a2(x))
    x = _pool(_activation(self.conv_a3(x)))
    x = _activation(self.conv_a4(x))
    x = _pool(_activation(self.conv_a5(x)))
    x = _activation(self.conv_a6(x))
    x = _pool(_activation(self.conv_a7(x)))
    x = _activation(self.conv_a8(x))
    img_size = x.shape[2]
    img_channel = x.shape[1]
    x = x.view(n_batch, n_max_images, img_channel, img_size, img_size)
    return x

##################################################################################################

class Policy(nn.Module):
  def __init__(self):
    super().__init__()
    self.image_conv = ImageConv()
    self.conv_ac1 = _weight_norm(nn.Conv2d(259, 256, 3, padding=1, padding_mode=_padding_mode))
    self.conv_ac2 = _weight_norm(nn.Conv2d(256, 128, 3, padding=1, padding_mode=_padding_mode))
    self.conv_ac3 = _weight_norm(nn.Conv2d(128, 64, 3, padding=1,  padding_mode=_padding_mode))
    self.conv_ac4 = _weight_norm(nn.Conv2d(64, 32, 3, padding=1,  padding_mode=_padding_mode))
    self.flatten = nn.Flatten()
    self.linear = _weight_norm(nn.Linear(2048, 1024))
    self.pos_encoder = PositionalEncoding(d_model=1024, dropout=0.0, max_len=20)
    self.transformer = nn.Transformer(nhead=8, d_model=1024, num_encoder_layers=12, num_decoder_layers=12)
    self.linear_1 = _weight_norm(nn.Linear(1024, 512))
    self.linear_2 = _weight_norm(nn.Linear(512, 64))
    self.mean = _weight_norm(nn.Linear(64, 2))
    self.log = _weight_norm(nn.Linear(64, 2))

  def forward(self, images, light_dirs):
    images = self.image_conv(images)
    n_batch, n_max_images = images.shape[0], images.shape[1]
    lights = light_dirs.unsqueeze(-1).unsqueeze(-1).repeat(1, 1, 1,images.shape[3], images.shape[4])
    x = torch.cat([images, lights], dim=2)
    x = x.view(n_batch * n_max_images, -1, images.shape[3], images.shape[4])
    x = _activation(self.conv_ac1(x))
    x = _activation(self.conv_ac2(x))
    x = _activation(self.conv_ac3(x))
    x = _activation(self.conv_ac4(x))
    x = self.flatten(x) #(B*seq, 1024)
    x = self.linear(x)
    x = x.view(n_batch, n_max_images, -1)
    x = x.transpose(0, 1)
    x = self.pos_encoder(x)
    dummy_tgt = torch.zeros_like(x)
    x = self.transformer(x, dummy_tgt)
    x = x.transpose(0, 1)
    x = x.mean(dim=1)
    x = self.linear_1(x)
    x = self.linear_2(x)

    mean = self.mean(x)
    log_std = self.log(x)
    log_std = torch.clamp(log_std, min=-20, max=2)
    return mean, log_std
##################################################################################################
### ANET   ###

class ANet(nn.Module):
  def __init__(self):
    super().__init__()
    self.conv_b1 = _weight_norm(nn.Conv2d(64, 64, 3, padding=1, padding_mode=_padding_mode))
    self.conv_b2 = _weight_norm(nn.Conv2d(64, 64, 3, padding=1, padding_mode=_padding_mode))
    self.conv_b3 = _weight_norm(nn.Conv2d(64, 128, 3, padding=1, padding_mode=_padding_mode))
    self.conv_b4 = _weight_norm(nn.Conv2d(128, 128, 3, padding=1, padding_mode=_padding_mode))
    self.conv_b5 = _weight_norm(nn.Conv2d(128, 256, 3, padding=1, padding_mode=_padding_mode))
    self.conv_b6 = _weight_norm(nn.Conv2d(256, 256, 3, padding=1, padding_mode=_padding_mode))
    self.conv_trans_b1 = _weight_norm(nn.ConvTranspose2d(256, 128, 3, stride=2, padding=1, output_padding=1))
    self.conv_c1 = _weight_norm(nn.Conv2d(256, 256, 3, padding=1, padding_mode=_padding_mode))
    self.conv_c2 = _weight_norm(nn.Conv2d(256, 128, 3, padding=1, padding_mode=_padding_mode))
    self.conv_trans_b2 = _weight_norm(nn.ConvTranspose2d(128, 64, 3, stride=2, padding=1, output_padding=1))
    self.conv_c3 = _weight_norm(nn.Conv2d(128, 128, 3, padding=1, padding_mode=_padding_mode))
    self.conv_c4 = _weight_norm(nn.Conv2d(128, 128, 3, padding=1, padding_mode=_padding_mode))
    self.conv_trans_b3 = _weight_norm(nn.ConvTranspose2d(128, 64, 3, stride=2, padding=1, output_padding=1))
    self.conv_c5 = _weight_norm(nn.Conv2d(64, 64, 3, padding=1, padding_mode=_padding_mode))
    self.conv_c6 = _weight_norm(nn.Conv2d(64, 1, 3, padding=1, padding_mode=_padding_mode))

  def forward(self, images, light_dirs):
    images = images.permute(0, 1, 3, 4, 2)
    n_batch, n_max_images, img_size, _, _ = images.shape
    img_channel = 64
    x = torch.zeros(images.shape[:-1] + (img_channel, (_n_light_axix//2)**2), device=images.device)
    for i_image in range(n_max_images):
      x[:, i_image, :, :, 0, :] = 1.0 - 0.9 ** (i_image + 1)
    for i_batch in range(n_batch):
      for i_image in range(n_max_images):
        x[i_batch, i_image:, :, :, :, light_dirs[i_batch, i_image]] += \
          images[None, i_batch, i_image, :, :, :img_channel]
    x = x.view(n_batch * n_max_images * img_size * img_size, img_channel, _n_light_axix//2, _n_light_axix//2)
    x = _activation(self.conv_b1(x))
    x = _activation(self.conv_b2(x))
    x_1 = x
    x = _pool(x)
    x = _activation(self.conv_b3(x))
    x = _activation(self.conv_b4(x))
    x_2 = x
    x = _pool(x)
    x = _activation(self.conv_b5(x))
    x = _activation(self.conv_b6(x))
    x = _activation(self.conv_trans_b1(x))
    x = torch.cat([x_2, x], dim=1)
    x = _activation(self.conv_c1(x))
    x = _activation(self.conv_c2(x))
    x = _activation(self.conv_trans_b2(x))
    x = torch.cat([x_1, x], dim=1)
    x = _activation(self.conv_c3(x))
    x = _activation(self.conv_c4(x))
    x = _activation(self.conv_trans_b3(x))
    x = _activation(self.conv_c5(x))
    x = self.conv_c6(x)
    x = x.view(n_batch, n_max_images, img_size, img_size, _n_light_axix, _n_light_axix)
    return x
##################################################################################################
### VNET   ###
class VNet(nn.Module):
  def __init__(self):
    super().__init__()
    self.conv_a1 = _weight_norm(nn.Conv2d(512, 512, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a2 = _weight_norm(nn.Conv2d(512, 512, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a3 = _weight_norm(nn.Conv2d(512, 512, 3, padding=1, padding_mode=_padding_mode))
    self.conv_a4 = _weight_norm(nn.Conv2d(512, 1, 3, padding=1, padding_mode=_padding_mode))

  def forward(self, images):
    n_batch, n_max_images, img_channel, img_size, _ = images.shape
    x = torch.zeros([n_batch, n_max_images, img_channel*2, img_size, img_size], device=images.device)
    for i_image in range(n_max_images):
      x[:, i_image, :img_channel, :, :] = images[:, :(i_image+1), :, :, :].mean(dim=1)
      x[:, i_image, img_channel:, :, :] = images[:, :(i_image+1), :, :, :].max(dim=1)[0]
      x[:, i_image, 0, :, :] = 1.0 - 0.9 ** (i_image + 1)
    x = x.view(n_batch * n_max_images, img_channel*2, img_size, img_size)
    x = _activation(self.conv_a1(x))
    x = _activation(self.conv_a2(x))
    x = _activation(self.conv_a3(x))
    x = self.conv_a4(x)
    x = x.view(n_batch, n_max_images, img_size, img_size)
    return x
##################################################################################################
### QNET   ###
class QNet(nn.Module):
  def __init__(self):
    super().__init__()
    self.image_conv = ImageConv()
    self.anet = ANet()
    self.vnet = VNet()

  def forward(self, images, light_dirs):
    images = self.image_conv(images)
    x_a = self.anet(images, light_dirs)
    x_v = self.vnet(images)
    x = x_a - x_a.mean(dim=(-2, -1), keepdim=True) + x_v[..., None, None]
    return x

##################################################################################################
### QNET_ENHANCE   ###
class QNetEnhance(nn.Module):
  def __init__(self):
    super().__init__()
    self.qnet = QNet()
    self.grad_id = 0

  def transform_to(self, images, light_dirs, transform_id):
    light_dirs_x = light_dirs // (_n_light_axix // 2)
    light_dirs_y = light_dirs % (_n_light_axix // 2)
    if transform_id == 0:
      return images, light_dirs
    elif transform_id == 1:
      light_dirs_x, light_dirs_y = light_dirs_x, (_n_light_axix // 2) - 1 - light_dirs_y
      return images.flip(dims=[4]), light_dirs_x * (_n_light_axix // 2) + light_dirs_y
    elif transform_id == 2:
      light_dirs_x, light_dirs_y = (_n_light_axix // 2) - 1 - light_dirs_x, light_dirs_y
      return images.flip(dims=[3]), light_dirs_x * (_n_light_axix // 2) + light_dirs_y
    elif transform_id == 3:
      light_dirs_x, light_dirs_y = (_n_light_axix // 2) - 1 - light_dirs_x, (_n_light_axix // 2) - 1 - light_dirs_y
      return images.flip(dims=[3, 4]), light_dirs_x * (_n_light_axix // 2) + light_dirs_y
    elif transform_id >= 4 and transform_id < 8:
      light_dirs_x, light_dirs_y = light_dirs_y, light_dirs_x
      light_dirs = light_dirs_x * (_n_light_axix // 2) + light_dirs_y
      return self.transform_to(images.transpose(3, 4), light_dirs, transform_id - 4)

  def transform_from(self, q_vals, transform_id):
    if transform_id == 0:
      return q_vals
    elif transform_id == 1:
      return q_vals.flip(dims=[3, 5])
    elif transform_id == 2:
      return q_vals.flip(dims=[2, 4])
    elif transform_id == 3:
      return q_vals.flip(dims=[2, 3, 4, 5])
    elif transform_id >= 4 and transform_id < 8:
      q_vals = self.transform_from(q_vals, transform_id - 4)
      return q_vals.transpose(2, 3).transpose(4, 5)

  def forward(self, images_orig, light_dirs_orig):
    n_batch = images_orig.shape[0]
    with torch.no_grad():
      transform_range = 8
      state = [self.transform_to(images_orig, light_dirs_orig, transform_id) for transform_id in range(transform_range)]
      images, light_dirs = [torch.cat(curr_state) for curr_state in zip(*state)]
      q_vals = self.qnet(images, light_dirs)
      q_vals = q_vals.view([transform_range, n_batch] + list(q_vals.shape[1:]))
    state = [self.transform_to(images_orig, light_dirs_orig, self.grad_id)]
    images, light_dirs = [torch.cat(curr_state) for curr_state in zip(*state)]
    q_vals = q_vals.clone()
    q_vals[self.grad_id] = self.qnet(images, light_dirs)
    self.grad_id = (self.grad_id + 1) % transform_range
    q_vals = torch.stack([self.transform_from(q_vals[i, ...], i) for i in range(transform_range)])
    q_vals = q_vals.mean(dim=0)
    return q_vals